<?php

namespace Drupal\workspaces_route_lock\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Workspaces route lock entities.
 */
interface WorkspacesRouteLockInterface extends ConfigEntityInterface {

  /**
   * Checks if this route lock item applies to the given route id.
   *
   * @param string $routeId
   *   The route id.
   *
   * @return bool
   *   True if the entity applies to the route. False otherwise.
   */
  public function appliesToRoute(string $routeId);

  /**
   * Tells if a given workspace is allowed on this route.
   *
   * @param string $workspaceId
   *   The id of the workspace.
   *
   * @return bool
   *   True if the workspace is allowed. False otherwise.
   */
  public function workspaceIsAllowed(string $workspaceId);

  /**
   * Returns the first workspace from the available workspaces list.
   *
   * @return \Drupal\workspaces\WorkspaceInterface||null
   *   The workspace object.
   * @throws \Drupal\workspaces_route_lock\Entity\CannotLoadWorkspaceException
   */
  public function getWorkspaceToSwitchTo();

  /**
   * Returns the ids of allowed workspaces.
   *
   * @return String[]
   *   An array of workspace ids.
   */
  public function getWorkspaces();

  /**
   * Returns the route name or pattern.
   *
   * @return string
   *   The route name or pattern.
   */
  public function getRoutePattern();

}
